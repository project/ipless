<?php

namespace Drupal\ipless_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Ipless test controller.
 */
class IplessTestController extends ControllerBase {

  /**
   * Test page for ipless.
   *
   * @return array
   *   The array renderable.
   */
  public function lessTestPage() {
    return [
      '#type' => 'container',
      '#markup' => '<h1>Ip{less} Test</h1><br><h2>This text must be green.</h2>',
      '#attached' => [
        'library' => [
          'ipless_test/ipless_test.base',
        ],
      ],
    ];
  }

}
