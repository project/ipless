<?php

namespace Drupal\ipless_test\EventSubscriber;

use Drupal\ipless\Event\IplessCompilationEvent;
use Drupal\ipless\Event\IplessEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * EventSubscriber to test ipless events.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      IplessEvents::LESS_FILE_COMPILED => [['eventLessFileCompiled', 0]],
    ];
  }

  /**
   * Event callback: Adds custom variable when file is compiled.
   *
   * @param \Drupal\ipless\Event\IplessCompilationEvent $event
   *   The ipless event object.
   */
  public function eventLessFileCompiled(IplessCompilationEvent $event) {
    $event->getLess()
      ->ModifyVars(['custom_variable' => "Ipless custom variable"]);
  }

}
