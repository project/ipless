<?php

namespace Drupal\Tests\ipless\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for ipless compilation.
 *
 * @group ipless
 */
class IplessTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'ipless',
    'ipless_test',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the Ipless compilation works.
   */
  public function testIplessCompilation() {

    // Enable all ipless options.
    $config = $this->config('system.performance');
    $config->set('ipless.enabled', TRUE);
    $config->set('ipless.modedev', TRUE);
    $config->set('ipless.sourcemap', TRUE);
    $config->set('ipless.watch_mode', TRUE);
    $config->save();

    $this->drupalGet(Url::fromRoute('ipless_test.test'));
    $this->assertSession()
      ->responseContains('<h1>Ip{less} Test</h1>');

    $this->assertSession()
      ->responseContains('ipless/ipless_test-ipless_test.base--main.css');

    $file = $this->publicFilesDirectory . '/ipless/ipless_test-ipless_test.base--main.css';
    $this->assertFileExists($file, 'The main.less file compiled with all options enabled');
    $content = file_get_contents($file);

    // Check compilation.
    $this->assertStringContainsString('color: #840b0b;', $content, 'Test ipless compilation');

    // Check custom variable.
    $this->assertStringContainsString('content: Ipless custom variable;', $content, 'Test ipless compilation with custom variable');

    // Check sourcemap.
    $this->assertStringContainsString('sourceMappingURL=data:application/json', $content, 'Test ipless sourcemap enabled');

    // Source map disabled.
    $config->set('ipless.sourcemap', FALSE)->save();
    drupal_flush_all_caches();
    $this->drupalGet(Url::fromRoute('ipless_test.test'));
    $this->assertFileExists($file, 'The main.less file sourcemap disabled');

    $content = file_get_contents($file);
    $this->assertStringNotContainsString('sourceMappingURL=data:application/json', $content, 'Test ipless sourcemap disabled');

    // Disable.
    $config->set('ipless.enabled', FALSE)->save();
    drupal_flush_all_caches();
    $this->drupalGet(Url::fromRoute('ipless_test.test'));
    $this->assertSession()
      ->responseNotContains('ipless/ipless_test-ipless_test.base--main.css');
  }

}
