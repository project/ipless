<?php

namespace Drupal\ipless\Asset;

/**
 * Interface AssetRendererInterface represent the asset renderer.
 */
interface AssetRendererInterface {

  /**
   * Render method.
   *
   * @param array $libraries
   *   Array of libraries.
   * @param int|null $time
   *   Timestamp, is set, only file edited after this date is generated.
   *
   * @return array
   *   The list of libraries compiled.
   */
  public function render(array $libraries, ?int $time = NULL): array;

  /**
   * Return Less processor.
   *
   * @param bool $reset
   *   Reset the less object. This will clear all variable of the less object.
   *
   * @return \Less_Parser
   *   Return instance of Less_Parser.
   */
  public function getLess(bool $reset = FALSE): \Less_Parser;

}
