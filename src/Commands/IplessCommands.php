<?php

namespace Drupal\ipless\Commands;

use Drupal\ipless\Ipless;
use Drush\Commands\DrushCommands;

/**
 * Ipless drush commands.
 */
class IplessCommands extends DrushCommands {

  /**
   * Ipless service.
   *
   * @var \Drupal\ipless\Ipless
   */
  protected $ipless;

  /**
   * Constructs a new IplessCommands object.
   *
   * @param \Drupal\ipless\Ipless $ipless
   *   Ipless service.
   */
  public function __construct(Ipless $ipless) {
    parent::__construct();
    $this->ipless = $ipless;
  }

  /**
   * Generate Simple Less CSS files.
   *
   * @command ipless:generate
   * @usage drush ipless:generate.
   * @aliases ipless
   */
  public function generate() {
    $this->ipless->generateAllLibraries();
  }

}
