<?php

namespace Drupal\ipless\Event;

use Drupal\ipless\Asset\AssetRenderer;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * IplessCompilationEvent used to indicate new compilation.
 */
class IplessCompilationEvent extends Event {

  /**
   * Less AssetRenderer.
   *
   * @var \Drupal\ipless\Asset\AssetRenderer
   */
  protected $assetRender;

  /**
   * Constructs a new IplessCompilationEvent object.
   *
   * @param \Drupal\ipless\Asset\AssetRenderer $asset_renderer
   *   Less AssetRenderer.
   */
  public function __construct(AssetRenderer $asset_renderer) {
    $this->assetRender = $asset_renderer;
  }

  /**
   * Return Less Processor.
   *
   * @return \Less_Parser
   *   Less parser object.
   */
  public function getLess() {
    return $this->assetRender->getLess();
  }

}
