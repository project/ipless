<?php

namespace Drupal\ipless\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * IpLessSettingForm is an helper class used in form alter hook.
 *
 * @see ipless_form_alter().
 */
class IpLessSettingForm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new IpLessSettingForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Form Callback : Check settings.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function checkSettings(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('system.performance');

    $form_state->cleanValues();
    $config->set('ipless', $form_state->getValue('ipless'));
    $config->save();
  }

  /**
   * Form Alter.
   *
   * @param array $form
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function formAlterSystemPerformanceSettings(&$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('system.performance');

    $form['bandwidth_optimization']['ipless'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Less CSS'),
      '#tree' => TRUE,
    ];

    $form['bandwidth_optimization']['ipless']['enabled'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('ipless.enabled'),
      '#title' => $this->t('Less compilation enabled'),
    ];

    $form['bandwidth_optimization']['ipless']['modedev'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('ipless.modedev'),
      '#title' => $this->t('Less developer mode'),
      '#description' => $this->t('Compile Less file all the time.'),
    ];

    $form['bandwidth_optimization']['ipless']['sourcemap'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('ipless.sourcemap'),
      '#title' => $this->t('Enable SourceMap'),
      '#states' => [
        'enabled' => [
          ':input[name="ipless[modedev]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['bandwidth_optimization']['ipless']['watch_mode'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('ipless.watch_mode'),
      '#title' => $this->t('Enable watch mode'),
      '#description' => $this->t('Refreshed CSS sheets without needing to reload the page.'),
      '#states' => [
        'enabled' => [
          ':input[name="ipless[modedev]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['#submit'][] = [$this, 'checkSettings'];
  }

}
