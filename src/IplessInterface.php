<?php

namespace Drupal\ipless;

use Drupal\Core\Render\HtmlResponse;

/**
 * IplessInterface for ipless service.
 */
interface IplessInterface {

  /**
   * Check configuration and generate {Less} files.
   *
   * @param array $libraries
   *   Array of libraries to generate. [0 => foo/bar, 1 => example/example].
   * @param int|null $time
   *   If set only library edited after this time was generated.
   *
   * @return array
   *   The list of libraries compiled.
   */
  public function generate(array $libraries, $time = NULL): array;

  /**
   * Flush all compiled files.
   */
  public function flushFiles(): void;

  /**
   * Compile all Less files on HTML response.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The html response object.
   */
  public function processOnResponse(HtmlResponse $response);

  /**
   * Ask for rebuild all libraries.
   *
   * @param bool $rebuild_need
   *   Indicate if the CSS needs to be rebuilt.
   */
  public function askForRebuild(bool $rebuild_need = TRUE): void;

  /**
   * Compile Less file present on all libraries.
   */
  public function generateAllLibraries();

  /**
   * Return true if the libraries must be rebuilt.
   *
   * @return bool
   *   Return true if rebuild is needs.
   */
  public function mustRebuildAll(): bool;

  /**
   * Indicate if the LESS compilation is enabled.
   *
   * @return bool
   *   Return true if ipless is enabled or false.
   */
  public function isEnabled(): bool;

  /**
   * Indicate if the watch mode is enabled.
   *
   * @return bool
   *   Return true if watch mode is enabled.
   */
  public function isWatchModeEnable(): bool;

  /**
   * Indicate if the LESS dev mode is enabled.
   *
   * @return bool
   *   Return true if dev mode is enabled.
   */
  public function isModeDevEnabled(): bool;

}
