<?php

namespace Drupal\ipless;

use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\State\StateInterface;
use Drupal\ipless\Asset\AssetRendererInterface;

/**
 * Description of Ipless.
 */
class Ipless implements IplessInterface {

  use MessengerTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Asset renderer service.
   *
   * @var \Drupal\ipless\Asset\AssetRendererInterface
   */
  protected $assetRenderer;

  /**
   * Library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Theme list service.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeList;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new Ipless object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\ipless\Asset\AssetRendererInterface $assetRenderer
   *   The asset renderer service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The library discovery service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeList
   *   The theme list service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, AssetRendererInterface $assetRenderer, LibraryDiscoveryInterface $libraryDiscovery, ModuleHandlerInterface $moduleHandler, ThemeExtensionList $themeList, FileSystemInterface $fileSystem, StateInterface $state) {
    $this->configFactory = $configFactory;
    $this->assetRenderer = $assetRenderer;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->moduleHandler = $moduleHandler;
    $this->themeList = $themeList;
    $this->fileSystem = $fileSystem;
    $this->state = $state;

    $this->config = $this->configFactory->get('system.performance');
  }

  /**
   * {@inheritdoc}
   */
  public function processOnResponse(HtmlResponse $response) {
    $assets = $this->getResponseAssets($response);
    $this->generate($assets->getLibraries());
  }

  /**
   * Return the attached assets object.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The html response object.
   *
   * @return \Drupal\Core\Asset\AttachedAssets
   *   The attached assets object.
   */
  public function getResponseAssets(HtmlResponse $response) {
    $attached = $response->getAttachments();

    unset($attached['html_response_attachment_placeholders']);

    return AttachedAssets::createFromRenderArray(['#attached' => $attached]);
  }

  /**
   * {@inheritdoc}
   */
  public function generate(array $libraries, $time = NULL): array {
    if (!$this->checkLib()) {
      return [];
    }
    return $this->generateCss($libraries, $time);
  }

  /**
   * Check that the library Less php is installed.
   *
   * @return bool
   *   Return true if the Less library is installed, false if not.
   */
  protected function checkLib() {
    if (!class_exists('Less_Parser')) {
      $this->messenger()->addWarning('The class lessc is not installed.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function mustRebuildAll(): bool {
    return (bool) $this->state->get('ipless.force_rebuild');
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->config->get('ipless.enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function isWatchModeEnable(): bool {
    return $this->isModeDevEnabled() && $this->config->get('ipless.watch_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function isModeDevEnabled(): bool {
    return (bool) $this->config->get('ipless.modedev');
  }

  /**
   * Generate Less files.
   *
   * @param array $libraries
   *   Array of libraries to compile.
   * @param int|null $time
   *   Timestamp, is set, only file edited after this date is generated.
   *
   * @return array
   *   The list of libraries generated.
   */
  protected function generateCss(array $libraries, ?int $time = NULL): array {
    return $this->assetRenderer->render($libraries, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function generateAllLibraries() {

    $modules = $this->moduleHandler->getModuleList();
    $themes = $this->themeList->reset()->getList();

    $extensions = array_merge($modules, $themes);

    $libraries = [];
    foreach (array_keys($extensions) as $extension_name) {
      $ext_libs = $this->libraryDiscovery->getLibrariesByExtension($extension_name);
      foreach ($ext_libs as $library_name => $lib_info) {
        if ($library_name !== 'drupalSettings') {
          $libraries[] = "$extension_name/$library_name";
        }
      }
    }

    $this->generate($libraries);
    // Disable the rebuild.
    $this->askForRebuild(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function askForRebuild(bool $rebuild_need = TRUE): void {
    $this->state->set('ipless.force_rebuild', $rebuild_need);
  }

  /**
   * {@inheritdoc}
   */
  public function flushFiles(): void {
    $this->fileSystem->deleteRecursive('public://ipless/');
  }

}
