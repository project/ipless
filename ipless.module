<?php

/**
 * @file
 * Hook implementations for the ipless module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\ipless\Form\IpLessSettingForm;

/**
 * Implements hook_help().
 */
function ipless_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.ipless':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_config_schema_info_alter().
 */
function ipless_config_schema_info_alter(&$definitions) {
  $definitions['system.performance']['mapping']['ipless'] = [
    'type' => 'mapping',
    'label' => 'iPless',
    'mapping' => [
      'enabled' => [
        'type' => 'boolean',
        'label' => 'Enabled',
      ],
      'modedev' => [
        'type' => 'boolean',
        'label' => 'Dev mode',
      ],
      'sourcemap' => [
        'type' => 'boolean',
        'label' => 'Source map',
      ],
      'watch_mode' => [
        'type' => 'boolean',
        'label' => 'Watch mode',
      ],
    ],
  ];

}

/**
 * Implements hook_cache_flush().
 */
function ipless_cache_flush() {
  /** @var \Drupal\ipless\Ipless $ipless */
  $ipless = \Drupal::service('ipless.base');

  // Flush all CSS files.
  $ipless->flushFiles();
  $ipless->askForRebuild();
}

/**
 * Implements hook_rebuild().
 */
function ipless_rebuild() {
  /** @var \Drupal\ipless\Ipless $ipless */
  $ipless = \Drupal::service('ipless.base');
  if ($ipless->isEnabled()) {
    $ipless->generateAllLibraries();
  }
}

/**
 * Implements hook_cache_flush().
 */
function ipless_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id !== 'system_performance_settings') {
    return;
  }

  // Use custom class IpLessSettingsForm to alter the system performance
  // settings.
  $form_ipless = \Drupal::classResolver(IpLessSettingForm::class);
  $form_ipless->formAlterSystemPerformanceSettings($form, $form_state);
}

/**
 * Implements hook_page_attachments().
 */
function ipless_page_attachments(array &$attachments) {
  /** @var \Drupal\ipless\Ipless $ipless */
  $ipless = \Drupal::service('ipless.base');
  if ($ipless->isWatchModeEnable()) {
    $attachments['#attached']['library'][] = 'ipless/ipless.watching';
  }
}

/**
 * Implements hook_library_info_alter().
 */
function ipless_library_info_alter(&$libraries, $extension) {

  $ipless_enabled = \Drupal::service('ipless.base')->isEnabled();

  // Look for the configuration of less to complete them.
  foreach ($libraries as $library => &$library_data) {

    foreach ($library_data as $key => &$data) {
      // If no Less continue.
      if ($key !== 'less') {
        continue;
      }

      if (!$ipless_enabled) {
        // Remove the less definition unused.
        unset($library_data['less']);

        // Check if the libraries defined only less file.
        if (empty($libraries['css']) && empty($libraries['js'])) {
          unset($libraries[$library]);
        }

        continue;
      }

      foreach ($data as $group => &$files) {
        foreach ($files as $file_less => &$file_conf) {
          // Escape if the output is already configured.
          if (!empty($file_conf['output'])) {
            continue;
          }

          // Remove the old output from css configuration.
          if (!empty($file_conf['output']) && isset($libraries[$library]['css'][$group][$file_conf['output']])) {
            unset($libraries[$library]['css'][$group][$file_conf['output']]);
          }

          $file_parsed = pathinfo($file_less);
          $uri = "public://ipless/{$extension}-{$library}--{$file_parsed['filename']}.css";

          // Adds output to the less definition.
          $file_conf['output'] = $uri;

          // Adds the new generated file to css.
          $library_data['css'][$group][$uri] = $file_conf;
        }
      }
    }
  }
}
