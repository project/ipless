(function (Drupal, once) {
  Drupal.behaviors.iplessWatching = {
    attach(context, settings) {
      const base = this;

      const elements = once('ipless_watching', 'body', context);

      elements.forEach(function (e) {
        base.init(settings.ipless).buildUi(context).setWatch();
      });
    },
    init(options) {
      if (this.getCookie('ipless.disabled')) {
        this.status = 0;
      } else {
        this.status = this.options.default_status;
      }

      this.options = { ...this.options, ...options };
      return this;
    },
    watch() {
      if (!this.getStatus()) {
        return;
      }
      const base = this;
      Drupal.ajax({
        url: '/ipless/watching',
        submit: {
          libraries: this.getLibrariesKeys(),
          time: this.getLastModifiedTime(),
        },
        success(data) {
          Object.keys(data).forEach(function (key, i) {
            const library = data[key];
            base.updateLastModifiedTime(library.library);

            const out = library.output.replace('public://', '');
            const href = document
              .querySelector(`link[href*="${out}"`)
              .getAttribute('href');
            const url = new URL(href, window.location.href);
            document
              .querySelector(`link[href="${href}"`)
              .setAttribute('href', `${url.pathname}?rev=${Date.now()}`);
          });

          base.setError(false).setWatch();
        },
        error(response) {
          let error = response.responseText;
          if (
            response.responseJSON !== undefined &&
            response.responseJSON.error !== undefined
          ) {
            error = response.responseJSON.error;
          }
          base.setError(error).setWatch();
        },
      }).execute();
    },
    setWatch() {
      const me = this;
      this.timer = setTimeout(function () {
        me.watch();
      }, this.options.refresh_rate);
      return this;
    },
    clearWatch() {
      clearTimeout(this.timer);
    },
    getLastModifiedTime() {
      let lastModificationTime = 0;
      const libs = this.getLibraries();
      Object.keys(libs).forEach(function (key, i) {
        const library = libs[key];
        if (library.last_m_time > lastModificationTime) {
          lastModificationTime = library.last_m_time;
        }
      });
      return lastModificationTime;
    },
    updateLastModifiedTime(library) {
      const base = this;
      const time = Math.floor(Date.now() / 1000);
      const libs = this.getLibraries();
      Object.keys(libs).forEach(function (key, i) {
        if (libs[key].library === library) {
          base.options.libraries[key].last_m_time = time;
        }
      });
      return this;
    },
    getLibraries() {
      return this.options.libraries;
    },
    getLibrariesKeys() {
      const libraries = [];
      const libs = this.getLibraries();
      Object.keys(this.getLibraries()).forEach(function (key, i) {
        libraries.push(libs[key].library);
      });
      return libraries;
    },
    toggleStatus() {
      switch (this.getStatus()) {
        case 0:
          this.status = 1;
          this.setCookie('ipless.disabled', 0);
          this.setWatch();
          break;
        case 1:
          this.status = 0;
          this.setCookie('ipless.disabled', 1);
          this.clearWatch();
          break;
      }
      return this;
    },
    getStatus() {
      return this.status;
    },
    setError(message) {
      if (message === false) {
        this.error = 0;
        this.error_message = '';
      } else {
        this.error = 1;
        this.error_message = message;
      }
      this.uiRefreshStatus();
      return this;
    },
    buildUi(context) {
      const base = this;
      base.ui = document.createElement('div');
      base.ui.innerHTML = Drupal.theme('ipless_ui');
      context.querySelector('body').append(base.ui);

      base.uiRefreshStatus();

      base.ui.querySelector('.handle').addEventListener('click', function () {
        base.toggleStatus().uiRefreshStatus();
      });

      // Web profiler loaded.
      if (document.querySelectorAll('.sf-toolbar').length > 0) {
        base.ui.classList.add('profiler-loaded');
      }

      return this;
    },
    uiRefreshStatus() {
      const status = this.ui.querySelector('.status');
      switch (this.getStatus()) {
        case 0:
          status.classList.remove('play');
          status.classList.add('pause');
          break;
        case 1:
          status.classList.remove('pause');
          status.classList.add('play');
          break;
      }

      if (this.error) {
        status.classList.add('error');
        status.setAttribute('title', this.error_message);
      } else {
        status.classList.remove('error');
        status.setAttribute('title', '');
      }
      return this;
    },

    getCookie(name) {
      const value = `; ${document.cookie}`;
      const parts = value.split(`; ${name}=`);
      if (parts.length === 2) return parts.pop().split(';').shift();
    },

    setCookie(name, value, days) {
      document.cookie = `${name}=${value || ''}; path=/`;
    },

    status: 0,
    error: 0,
    error_message: '',
    timer: null,
    options: {
      libraries: [],
      refresh_rate: 2000,
      default_status: 1,
    },
  };

  Drupal.theme.ipless_ui = function () {
    return '<div class="ipless-ui"><div class="handle">{Less} <span class="status"></span></div></div>';
  };
})(Drupal, once);
